from django.shortcuts import render, get_object_or_404
from .models import Order, Rate
from django.shortcuts import redirect
from django.forms.models import model_to_dict
import datetime
from django.core.mail import send_mail
from .forms import CostForm, OrderForm
from django.template.defaulttags import register


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


def calc(r_hour, t, d_cons, d_coef, dis, bn, c_less, pr, props, an, dis_per, bg, bg_p, ch, ch_p):
    res = 0
    a = r_hour * t  # аренда
    if dis != 0:
        d = d_cons + d_coef * dis  # доставка
    else:
        d = 0
    if pr is True:
        p = props + an  # атрибуты+аниматор
    else:
        p = an
    s = a * dis_per / 100  # +-дисконт
    if bg is True:
        f = bg_p  # фон
    else:
        if ch is True:
            f = ch_p  # хромакей
        else:
            f = 0
    if bn is True:
        b = c_less * (a + d + p + s + f)  # безнал
    else:
        b = 0
    res = (a + d + b + p + s + f)  # итог
    return res, d


def cost(request):
    result = 0
    delivery = 0
    title = "Расчёт"
    if request.method == "POST":

        result_list = []
        form = CostForm(request.POST)
        rate = list(map(model_to_dict, Rate.objects.filter(current_tarif=True)))
        if form.is_valid():
            cost = form.save(commit=False)

            result_list = list(calc(r_hour=rate[0].get('rent_one_hour'),
                                    t=cost.time,
                                    d_cons=rate[0].get('delivery_const'),
                                    d_coef=rate[0].get('delivery_coef'),
                                    dis=cost.distance,
                                    bn=cost.cashless,
                                    c_less=rate[0].get('cashless_coef'),
                                    pr=cost.props,
                                    props=rate[0].get('props'),
                                    an=rate[0].get('animator'),
                                    dis_per=rate[0].get('discount_perс'),
                                    bg=cost.background,
                                    bg_p=rate[0].get('background'),
                                    ch=cost.chromakey,
                                    ch_p=rate[0].get('chromakey')))
            result = round(result_list[0])
            delivery = round(result_list[1])

            if 'order_submit' in request.POST:
                form = OrderForm(request.POST)
                context = {
                    'form': form,
                    'result': result,
                    'delivery': delivery,
                }
                return redirect(('/order/'), context=context)
    else:
        form = CostForm()

    context = {
        'form': form,
        'result': result,
        'delivery': delivery,
        'title': title,
    }
    return render(request, 'order/cost.html', context=context)


def order(request):
    title = "Бронирование"
    result = 0
    delivery = 0
    number = ''
    sent = False
    if request.method == "POST":

        result_list = []
        form = OrderForm(request.POST)
        rate = list(map(model_to_dict, Rate.objects.filter(current_tarif=True)))
        if form.is_valid():
            cost = form.save(commit=False)
          #  d = Rate.objects.get(current_tarif=True)
            result_list = list(calc(r_hour=rate[0].get('rent_one_hour'),
                                    t=cost.time,
                                    d_cons=rate[0].get('delivery_const'),
                                    d_coef=rate[0].get('delivery_coef'),
                                    dis=cost.distance,
                                    bn=cost.cashless,
                                    c_less=rate[0].get('cashless_coef'),
                                    pr=cost.props,
                                    props=rate[0].get('props'),
                                    an=rate[0].get('animator'),
                                    dis_per=rate[0].get('discount_perс'),
                                    bg=cost.background,
                                    bg_p=rate[0].get('background'),
                                    ch=cost.chromakey,
                                    ch_p=rate[0].get('chromakey')))
            result = round(result_list[0])
            delivery = round(result_list[1])
            print(rate[0].get('info'))
            print(rate[0].get('ID'))
            if 'order2_submit' in request.POST:

                cost.save()
                number = '{}-{}'.format(cost.id, datetime.date.today())
                cost.result = result
                cost.rate_id = int(rate[0].get('id'))
                cost.order_number = number
                # сохраняем в базу и отправляем почту клиенту и администратору
                cost.save()
                sent = True
                subject = 'Заказ № {}  на Photoidea.by'.format(cost.order_number)
                message = '{}, Ваш заказ отправлен на обработку.\n С вами свяжется администратор в ближайшее время.\n' \
                          ' Предварительная сумма аренды составляет: {} $ по курсу НБРБ на день оплаты\n' \
                          ' Включена доставка:{} $\n Дата заказа: {},Время аренды: {}часа \n' \
                          'Условия:{}'.format(cost.client_name, result,delivery, cost.date_event, cost.time,  rate[0].get('customer_info'))
                send_mail(subject, message, 'zhyhar.siarhei@gmail.com', [cost.client_email])
                sent = True

    else:
        form = OrderForm()

    context = {
        'form': form,
        'result': result,
        'delivery': delivery,
        'sent': sent,
        'number': number,
        'title': title,
    }

    return render(request, 'order/order.html', context=context)


# главная страница
def event(request):
    return render(request, 'order/event.html', {})
