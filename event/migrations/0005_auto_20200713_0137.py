# Generated by Django 3.0.7 on 2020-07-12 22:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0004_auto_20200711_1441'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='distance',
            field=models.SmallIntegerField(default=0, verbose_name='Расстояние от МКАД'),
        ),
    ]
