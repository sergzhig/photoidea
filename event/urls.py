from django.urls import path
from . import views
urlpatterns = [
    path('cost/', views.cost, name='cost'),
    path('order/', views.order, name='order'),
    path('', views.event, name='event'),
]