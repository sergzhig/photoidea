from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

User = get_user_model()


class Rate(models.Model):  # Модель Тариф , заполняет админ
    name_tarif = models.CharField("Название тарифа", max_length=50, blank=False, null=True)
    current_tarif = models.BooleanField("Тариф по умолчанию", default=False)
    rent_one_hour = models.SmallIntegerField("Стоимость 1 часа $", default=50)
    delivery_const = models.SmallIntegerField("Стоимость доставки min $", default=5)
    delivery_coef = models.FloatField("Стоимость доставки коэф.", default=0.2)
    cashless_coef = models.FloatField("Повышающий коэф. за безнал", default=0.1)
    background = models.SmallIntegerField("Стоимость фона $", default=10)
    props = models.SmallIntegerField("Стоимость атрибутов $", default=0)
    animator = models.SmallIntegerField("Стоимость аниматора $", default=0)
    discount_perс = models.SmallIntegerField("+-Дисконт %", default=0)
    chromakey = models.SmallIntegerField("Стоимость зеленого фона $", default=10)
    corp_email = models.EmailField("Корпоративные Email", max_length=50, blank=False, null=True)
    customer_info = models.TextField("Доп инф.клиенту на почту", max_length=2000, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.current_tarif:
            try:
                temp = Rate.objects.get(current_tarif=True)
                if self != temp:
                    temp.current_tarif = False
                    temp.save()
            except Rate.DoesNotExist:
                pass
        super(Rate, self).save(*args, **kwargs)

    def __str__(self):
        if self.current_tarif==True:
            t='Текущий(Основной)'
        else: t='Неактивный'
        return "%s-%s-%s" % (self.pk,self.name_tarif,t)


class Order(models.Model):  # Модель заказ , заполняет клиент
    order_number = models.CharField("Номер заказа", max_length=50, blank=True, null=True)
    client_name = models.CharField("Имя клиента", max_length=200, blank=True, null=True)
    client_tel = models.CharField("Телефон клиента", max_length=20)
    client_email = models.EmailField("емаил клиента", max_length=50, blank=False, null=True)
    date_event = models.DateField("Дата мероприятия", blank=True, null=True)
    time = models.SmallIntegerField("Время мероприятия,часов", blank=False, null=True)
    cashless = models.BooleanField("Безнал?", default=False)
    distance = models.SmallIntegerField("Расстояние от МКАД", default=0)
    background = models.BooleanField("Нужен ли фон?", default=False)
    chromakey = models.BooleanField("Нужен ли зеленый фон?", default=False)
    props = models.BooleanField("Нужен ли реквизит?", default=False)
    info = models.TextField("Дополнительная информация",default='количество гостей:\n'
                                                                'вид мероприятия:\n'
                                                                'особенности:\n',
                            max_length=2000, blank=True,
                            null=True)
    create = models.DateTimeField("Дата создания", max_length=200, default=timezone.now)
    rate = models.ForeignKey(Rate, null=True, blank=False, on_delete=models.PROTECT)
    result = models.FloatField("Итоговая цена", blank=True, null=True)


    class Booth(models.IntegerChoices):
        BLACK = 1  # черная
        WHITE = 2  # белая
        WOOD = 3  # деревяшка

    booth = models.IntegerField("Выбор вида будки", choices=Booth.choices)

    def __str__(self):
        return "%s-%s" % (self.order_number, self.client_name)


