from django import forms
from .models import Order


class CostForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('date_event', 'time', 'distance', 'cashless', 'background', 'chromakey', 'props')
        widgets = {
            'date_event': forms.DateInput(attrs={'type': 'date'})}


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = (
            'client_name', 'client_tel', 'client_email', 'date_event', 'time', 'distance', 'cashless',
            'background', 'chromakey', 'props', 'booth', 'info',)
        widgets = {
            'date_event': forms.DateInput(attrs={'type': 'date'})}
