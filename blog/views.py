from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.defaulttags import register
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django import views
from django.contrib.postgres.search import SearchVector,  SearchQuery, SearchRank
from .models import Post, Comment
from .forms import PostForm, CommentForm, RegistrationForm,  SearchForm
from .forms import EmailPostForm

User = get_user_model()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


month_dict = {
    1: "Январь",
    2: "Февраль",
    3: "Март",
    4: "Апрель",
    5: "Май",
    6: "Июнь",
    7: "Июль",
    8: "Август",
    9: "Сентябрь",
    10: "Октябрь",
    11: "Ноябрь",
    12: "Декабрь"
}


def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    posts_five = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')[0:4]
    dict_comm = {}
    for post in posts:
        n = Comment.objects.filter(page=post.pk).count()
        dict_comm[post.pk] = n

    paginator = Paginator(posts, 4)  # 4 поста на каждой странице
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # Если страница не является целым числом, поставим первую страницу
        posts = paginator.page(1)
    except EmptyPage:
        # Если страница больше максимальной, доставить последнюю страницу результатов
        posts = paginator.page(paginator.num_pages)

    context = {

        "title": "Страница блога",
        "posts": posts,
        "posts_five": posts_five,
        "arh": month_dict,
        "page": page,
        "dict_comm": dict_comm
    }
    return render(request, 'blog/post_list.html', context=context)


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    posts_five = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')[0:4]
    comments = Comment.objects.filter(page=pk)
    title = "Статья"
    context = {
        'post': post,
        'posts_five': posts_five,
        'title': title,
        'comments': comments,
        "arh": month_dict
    }

    return render(request, 'blog/post_detail.html', context=context)


def arhive_month(request, year, month):
    posts = Post.objects.filter(published_date__month=month).filter(published_date__year=year).order_by(
        '-published_date')
    title = "Архив" + " " + month_dict[month] + " " + str(year)
    return render(request, 'blog/arhive_month.html', {'posts': posts, 'title': title})


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()

    context = {
        'form': form,
        'title': 'new post'
    }

    return render(request, 'blog/post_edit.html', context=context)


def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.update_date = timezone.now()
            post.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    context = {
        'form': form,
        'title': 'edit post'
    }

    return render(request, 'blog/post_edit.html', context=context)


def comment_new(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.published_date = timezone.now()
            comment.save()
            return redirect('post_detail', pk=comment.page_id)
    else:
        form = CommentForm()
    context = {
        'form': form,
        'title': 'new comment'
    }

    return render(request, 'blog/comment_edit.html', context=context)


def about_me(request):
    context = {
        'title': 'about'
    }

    return render(request, 'blog/about_me.html', context=context)


def contact(request):
    sent = False
    if request.method == 'POST':
        # Форма была отправлена
        form = EmailPostForm(request.POST)
        if form.is_valid():
            # Поля формы прошли проверку
            cd = form.cleaned_data

            subject = 'mail from {} fo SergeyZhigar, back email:{}'.format(cd['name'], cd['email'])
            message = '{} wants to ask: {}'.format(cd['name'], cd['question'])
            send_mail(subject, message, 'zhyhar.siarhei@gmail.com', ['zhyhar.siarhei@gmail.com', cd['email']])
            sent = True
    else:
        form = EmailPostForm()
    context = {
        'form': form,
        'sent': sent
    }
    return render(request, 'blog/contact.html', context=context)


def post_share(request, pk):
    # Получить пост по id
    post = get_object_or_404(Post, pk=pk)
    sent = False
    if request.method == 'POST':
        # Форма была отправлена
        form = EmailPostForm(request.POST)
        if form.is_valid():
            # Поля формы прошли проверку
            cd = form.cleaned_data
            post_url = request.build_absolute_uri('/post/{}/'.format(post.pk))
            subject = '{} ({}) recommends you reading " {}"'.format(cd['name'], cd['email'], post.title)
            message = 'Read "{}" at {}\n\n{}\'s question: {}'.format(post.title, post_url, cd['name'], cd['question'])
            send_mail(subject, message, 'zhyhar.siarhei@gmail.com', [cd['to']])
            sent = True
    else:
        form = EmailPostForm()
    context = {
        'post': post,
        'form': form,
        'sent': sent
    }
    return render(request, 'blog/share.html', context=context)


class LoginClassBasedView(views.generic.edit.FormView):
    success_url = '/'
    form_class = AuthenticationForm
    template_name = "blog/login_form.html"

    def form_valid(self,form):
        user = form.get_user()
        login(self.request, user)
        if self.request.user.is_superuser:
            return redirect('/security/admin/')
        else:
            return redirect ('/')


def login_view(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('/')


def registration_view(request):
    if request.user.is_authenticated:
        return redirect('/security/admin/')
    if request.method == "POST":
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                password=form.cleaned_data['password'],
                email=form.cleaned_data['email'],
                username=form.cleaned_data['username']
            )

            return redirect('/blog/login/')
    else:
        form = RegistrationForm()
    context = {'form': form}
    return render(request, 'blog/login_form.html', context=context)


def post_search(request):
    form = SearchForm()
    query = None
    results = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            search_vector = SearchVector('title', weight='A') + SearchVector('text', weight='B')
            search_query = SearchQuery(query)
            results = Post.objects.annotate(
                rank=SearchRank(search_vector, search_query)
            ).filter(rank__gte=0.3).order_by('-rank')
    return render(request,'blog/search.html', {'form': form,'query': query,'results': results})
