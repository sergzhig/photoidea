from django import forms
from django.contrib.auth import get_user_model
from .models import Post, Comment
from django.core.files import File
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
User = get_user_model()

#censlist = ['жопа', 'porno', 'porn']


f = open(os.path.join(BASE_DIR, 'mat.txt'), 'r')
myfile = File(f)
for line in myfile.readlines():
    censlist = line.strip().split(',')


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'text',)

    def clean_text(self):
        t = self.cleaned_data['text']
        l = []
        for word in t.split(' '):
            if word.lower() in censlist:
                l.append('cens!')
            else:
                l.append(word)
        return ' '.join(l)

    def clean_title(self):
        t = self.cleaned_data['title']
        l = []
        for word in t.split(' '):
            if word.lower() in censlist:
                l.append('cens!')
            else:
                l.append(word)
        return ' '.join(l)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text', 'page',)

    def clean_text(self):
        t = self.cleaned_data['text']
        l = []
        for word in t.split(' '):
            if word.lower() in censlist:
                l.append('cens!')
            else:
                l.append(word)
        return ' '.join(l)


class EmailPostForm(forms.Form):
    name = forms.CharField(label='From Name', max_length=25)
    email = forms.EmailField(label='From email')
    to = forms.EmailField(label='Send to email', required=False)
    question = forms.CharField(label='You question', required=False, widget=forms.Textarea)

    def clean_question(self):
        que = self.cleaned_data['question']
        l = []
        for word in que.split(' '):
            if word.lower() in censlist:
                l.append('cens!')
            else:
                l.append(word)
        return ' '.join(l)


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'password', 'email', 'username']


class SearchForm(forms.Form):
    query = forms.CharField(label='введите текст для поиска')


